import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import productService from '@/services/product'
export const usePosStore = defineStore('pos', () => {
  const loadingStore = useLoadingStore()

  const product1 = ref<Product[]>([])
  const product2 = ref<Product[]>([])
  const product3 = ref<Product[]>([])
  async function getProducts() {
    try {
      loadingStore.doLoad()
      let res = await productService.getProductsByType(1)
      products1.value = res.data
      res = await productService.getProductsByType(2)
      products2.value = res.data
      res = await productService.getProductsByType(3)
      products3.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('Error')
      loadingStore.finish()
    }
  }
  return { product1, product2, product3, getProducts }
})
